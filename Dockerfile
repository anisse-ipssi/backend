FROM python:3.10.7-bullseye

WORKDIR /backend

RUN adduser appuser

COPY requirements.txt .

RUN pip3 install -r requirements.txt;

COPY . .

RUN chown -R appuser /backend

RUN chmod +x ./start.sh

USER appuser

CMD ["./start.sh"]
